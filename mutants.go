package main

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"github.com/bmizerany/pat"
	_ "github.com/mattn/go-sqlite3"
)

type Mutant struct {
  dna string `json:"dna"`
	isMutant bool `json:"isMutant"`
}

type Response struct {
	Message string `json:"message"`
}

type DNA struct {
	Dna []string `json:"dna"`
}

type Mutants []Mutant
type Responses []Response

var mainDB *sql.DB

func main() {

	db, errOpenDB := sql.Open("sqlite3", "mutants.db")
	checkErr(errOpenDB)
	mainDB = db

	r := pat.New()
	r.Get("/stats", http.HandlerFunc(statistics))
	r.Post("/mutant", http.HandlerFunc(checkMutant))

	http.Handle("/", r)

	log.Print(" Running on 9400")
	err := http.ListenAndServe(":9400", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func checkMutant(w http.ResponseWriter, r *http.Request) {
	// Read body
	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// Unmarshal
	var data DNA
	err = json.Unmarshal(b, &data)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	output, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	dna := data.Dna
	mutant := isMutant(dna)
	fmt.Println(mutant)

	w.Header().Set("content-type", "application/json")
	w.Write(output)
}

func statistics(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "%s", string("hola mundo stats!"))
}

func isMutant(dna []string) bool {
	mutant_block := checkBlocks(dna)
	if(len(mutant_block) > 1){
		return true
	}
	return false
}

func checkBlocks(dna []string) []string {
	horizontal := mutantChecker(dna);
	// vertical   := getVertical(dna);
	// vertical   := mutantChecker(getVertical(dna));
	//diagonal   := getDiagonals(dna);
	// diagonal   := mutantChecker(getDiagonals(dna));
	// inverse    := mutantChecker(getInverseDiagonals(dna));
    //return horizontal.concat(diagonal).concat(inverse);
	return horizontal
}

func mutantChecker(dna []string) []string{
	regex, _  := regexp.Compile("[A]{4}|[T]{4}|[C]{4}|[G]{4}")
	var found []string
	for i := 0; i < len(dna); i++ {
		result := regex.MatchString(dna[i])
		if(result) {
			found = append(found, dna[i])
		}
  }
	return found
}

// func getVertical(dna []string) []string{
// 	var vertical []string
// 	for i := 0; i < len(dna); i++ {
// 		for j := 0; j < len(dna[i]); j++ {
// 			fmt.Println(dna[i][j])
// 		}
// 	}
// 	return vertical
// }
//
// func getDiagonals(matrix []string) string {
// 	// Diagonal
// 	var output []string
// 	for i := 0; i < len(matrix); i++ {
// 		s := strings.Split(matrix[i],"")
// 		fmt.Println(matrix[i])
// 		for j := len(s) - 1; i < len(s); j-- {
// 			j := i
// 			output := append(output, s[j])
// 			i++
// 		}
// 	}
// 	fmt.Println(output)
// 	return "hola"
//
// }

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
