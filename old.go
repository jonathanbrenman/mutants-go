package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/bmizerany/pat"
	_ "github.com/mattn/go-sqlite3"
)

type Mutant struct {
  dna string `json:"dna"`
	isMutant boolean `json:"isMutant"`
}

type Mutants []Mutant

var mainDB *sql.DB

func main() {

	db, errOpenDB := sql.Open("sqlite3", "mutants.db")
	checkErr(errOpenDB)
	mainDB = db

	r := pat.New()
	r.Get("/stats", http.HandlerFunc(checkMutant))
	r.Post("/mutant", http.HandlerFunc(statistics))

	http.Handle("/", r)

	log.Print(" Running on 12345")
	err := http.ListenAndServe(":12345", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func checkMutant(w http.ResponseWriter, r *http.Request) {
	rows, err := mainDB.Query("SELECT * FROM mutants")
	checkErr(err)
	var mutants Mutants
	for rows.Next() {
		var mutant Mutant
		err = rows.Scan(&mutant.ID, &mutant.Name)
		checkErr(err)
		mutants = append(mutants, mutant)
	}
	jsonB, errMarshal := json.Marshal(mutants)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func statistics(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	var mutant Mutant
	mutant.Name = name
	stmt, err := mainDB.Prepare("INSERT INTO mutants(name) values (?)")
	checkErr(err)
	result, errExec := stmt.Exec(todo.Name)
	checkErr(errExec)
	newID, errLast := result.LastInsertId()
	checkErr(errLast)
	todo.ID = newID
	jsonB, errMarshal := json.Marshal(todo)
	checkErr(errMarshal)
	fmt.Fprintf(w, "%s", string(jsonB))
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
